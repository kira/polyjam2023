using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerController : MonoBehaviour
{
    [SerializeField] private LineRenderer _stalkRenderer;
    [SerializeField] private Transform _flowerHead;
    [SerializeField] private float _upDistance = 2f;
    [SerializeField] private float _sidesDistance = 5f;
    [SerializeField] private float _growSpeed = 1f;

    private bool _isInputBlocked;
    private Vector3 _startingPosition;
    private float _horizontalSpeed;

    // Start is called before the first frame update
    void Start()
    {
        _isInputBlocked = false;
        _startingPosition = _flowerHead.position;
        _horizontalSpeed = _growSpeed / 1.2f;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isInputBlocked == false && GameController.Instance.IsFlowerAlive)
        {
            if ((Input.GetKeyDown(KeyCode.LeftArrow) || VoiceController.Result == VoiceController.EResult.SILENT) 
                && _flowerHead.position.x - _sidesDistance > _startingPosition.x - GameController.Instance.MaxHorizontalMove)
            {
                AddPoint2Stalk();
                _isInputBlocked = true;
                StartCoroutine(FlowerGrowCor(_flowerHead.position + new Vector3(-_sidesDistance, _upDistance, 0f)));
            }
            if ((Input.GetKeyDown(KeyCode.RightArrow) || VoiceController.Result == VoiceController.EResult.LOUD) 
                && _flowerHead.position.x + _sidesDistance < _startingPosition.x + GameController.Instance.MaxHorizontalMove)
            {
                AddPoint2Stalk();
                _isInputBlocked = true;
                StartCoroutine(FlowerGrowCor(_flowerHead.position + new Vector3(_sidesDistance, _upDistance, 0f)));
            }
            Vector3 moveStep = new Vector3(0f, _growSpeed * Time.deltaTime, 0f);
            _flowerHead.position += moveStep;
            _stalkRenderer.SetPosition(_stalkRenderer.positionCount - 1, _flowerHead.position);
        }
    }

    private void AddPoint2Stalk()
    {
        _stalkRenderer.positionCount = _stalkRenderer.positionCount + 1;
        _stalkRenderer.SetPosition(_stalkRenderer.positionCount - 1, _flowerHead.position);
    }

    private IEnumerator FlowerGrowCor(Vector3 target)
    {
        float distance = (target - _flowerHead.position).magnitude;
        float moveTime = distance / _horizontalSpeed;
        float time = 0f;
        Vector3 startingPosition = _flowerHead.position;
        while(time < moveTime && GameController.Instance.IsFlowerAlive)
        {
            _flowerHead.position = Vector3.Lerp(startingPosition, target, time/moveTime);
            _stalkRenderer.SetPosition(_stalkRenderer.positionCount - 1, _flowerHead.position);
            time += Time.deltaTime * _horizontalSpeed;
            yield return null;
        }
        AddPoint2Stalk();
        _isInputBlocked = false;
    }
}
