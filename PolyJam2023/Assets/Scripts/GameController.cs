using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private float _maxHorizontalMove = 10f;
    public static GameController Instance { get; private set; }

    public bool IsFlowerAlive { get; set; }
    public float MaxHorizontalMove => _maxHorizontalMove;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }

        IsFlowerAlive = true;

        Time.timeScale = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Scenes/Main");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Time.timeScale = 1;
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
