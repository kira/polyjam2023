using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButterflyController : MonoBehaviour
{
    [SerializeField] private float _minMoveDistance;
    [SerializeField] private float _maxMoveDistance;
    [SerializeField] private float _moveSpeed = 0.5f;
    [SerializeField] private float _minWaitForNextMove;
    [SerializeField] private float _maxWaitForNextMove;
    private Vector3 _moveTarget;
    private float _waitTime;
    private float _elapsedWaitTime;

    // Start is called before the first frame update
    void Start()
    {
        _moveTarget = GetNewMoveTarget();
        _waitTime = GetNewWaitTime();
        _elapsedWaitTime = _waitTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, _moveTarget) > 0.001f)
        {
            float step = _moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, _moveTarget, step);
        }
        else if(_elapsedWaitTime > 0)
        {
            _elapsedWaitTime -= Time.deltaTime;
        }
        else
        {
            _moveTarget = GetNewMoveTarget();
            _waitTime = GetNewWaitTime();
        }
    }

    private Vector3 GetNewMoveTarget()
    {
        float newX, newY;
        newX = Random.Range(_minMoveDistance, _maxMoveDistance);
        newY = Random.Range(_minMoveDistance, _maxMoveDistance);
        if(Random.value > 0.5f) newX *= -1f;
        if (Random.value > 0.5f) newY *= -1f;
        Vector3 result = new Vector3(newX, newY, 0f);
        return transform.position + result;
    }

    private float GetNewWaitTime()
    {
        return Random.Range(_minWaitForNextMove, _maxWaitForNextMove);
    }
}
