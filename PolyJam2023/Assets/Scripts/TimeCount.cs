using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCount : MonoBehaviour
{
    private Text time;
    private float rTime = 0;
    
    // Start is called before the first frame update
    void Awake()
    {
        time = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

        try
        {
            if (GameController.Instance && GameController.Instance.IsFlowerAlive)
            {
                rTime += Time.deltaTime;
                GetComponent<Text>().text = ((int) rTime) + "";
            }
        }
        catch
        {
        }

    }
}

