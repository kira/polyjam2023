using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlowerBumper : MonoBehaviour
{
    private GameObject butterflyeScore;
    private int butterflyCollected = 0;



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            GameController.Instance.IsFlowerAlive = false;
        }
        else if (collision.gameObject.CompareTag("Butterfly"))
        {
            butterflyCollected += 1;
            GameObject.Find("ScoreCounter").GetComponent<Text>().text = "Butterflies: " + butterflyCollected;
            Destroy(collision.gameObject);
        }
    }

}
