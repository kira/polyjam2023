using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePlacer : MonoBehaviour
{
    [SerializeField] private Transform _obstaclePrefab;
    [SerializeField] private Transform _butterflyPrefab;
    [SerializeField] private Transform _backgroundPrefab;
    [SerializeField] private float _minTimeIntervalBetweenSpawns = 3;
    [SerializeField] private float _maxTimeIntervalBetweenSpawns = 10;
    [SerializeField] private float _timeIntervalBetweenFrontSpawns = 7f;
    [SerializeField] private Transform _obstaclesParent;
    [SerializeField] private float _spawnVerticalDistance = 10f;
    [SerializeField] private float _spawnHorizontalMaxDistance = 7f;
    [SerializeField][Range(0f, 1f)] private float obstacleToButterflyRatio = 0.5f;
    private float _time2NextSpawn;
    private float _time2NextSpawnFrontObstacle;
    private float _elapsedTime;
    private float _elapsedTimeFrontObstacle;
    private Vector3 _lastObstaclePosition;
    private Vector3 _lastBackgroundSpawnPosition;
    private Vector3 _startingPosition;

    // Start is called before the first frame update
    void Start()
    {
        _time2NextSpawn = 0f;
        _time2NextSpawnFrontObstacle = 0f;
        _elapsedTime = 0f;
        _elapsedTimeFrontObstacle = 0f;
        Random.InitState(System.DateTime.Now.Millisecond);
        _lastBackgroundSpawnPosition = new Vector3(-4.48f, 18.32f, 0f);
        _startingPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.Instance.IsFlowerAlive == false) return;
        _elapsedTime += Time.deltaTime;
        _elapsedTimeFrontObstacle += Time.deltaTime;
        if (_elapsedTime >= _time2NextSpawn)
        {
            Vector3 obstaclePosition = new Vector3(transform.position.x + Random.Range(-_spawnHorizontalMaxDistance, _spawnHorizontalMaxDistance), transform.position.y + _spawnVerticalDistance, 0f);
            if (Vector3.Distance(obstaclePosition, _lastObstaclePosition) > _obstaclePrefab.localScale.x * 2)
            {
                Transform newPrefab = Random.value > obstacleToButterflyRatio ? _butterflyPrefab : _obstaclePrefab;
                Instantiate(newPrefab, obstaclePosition, Quaternion.identity, _obstaclesParent);
                _elapsedTime = 0f;
                _time2NextSpawn = Random.Range(_minTimeIntervalBetweenSpawns, _maxTimeIntervalBetweenSpawns);
                _lastObstaclePosition = obstaclePosition;
            }
        }
        if (_elapsedTimeFrontObstacle >= _timeIntervalBetweenFrontSpawns)
        {
            Vector3 obstaclePosition = new Vector3(transform.position.x, transform.position.y + _spawnVerticalDistance, 0f);
            if (Vector3.Distance(obstaclePosition, _lastObstaclePosition) > _obstaclePrefab.localScale.x * 2)
            {
                Transform newPrefab = Random.value > obstacleToButterflyRatio ? _butterflyPrefab : _obstaclePrefab;
                Instantiate(newPrefab, obstaclePosition, Quaternion.identity, _obstaclesParent);
                _elapsedTimeFrontObstacle = 0f;
                _lastObstaclePosition = obstaclePosition;
            }
        }
        if(transform.position.y > _lastBackgroundSpawnPosition.y - 5f)
        {
            _lastBackgroundSpawnPosition = _startingPosition + new Vector3(0f, transform.position.y + 14f, 0f);
            Instantiate(_backgroundPrefab, _lastBackgroundSpawnPosition, Quaternion.identity);
        }
    }
}
