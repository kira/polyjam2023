using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadController : MonoBehaviour
{
    [SerializeField] private Animator _flowerAnimator;
    private float _startPosition;

    // Start is called before the first frame update
    void Start()
    {
        _startPosition = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x > _startPosition)
        {
            _flowerAnimator.ResetTrigger("Open");
            _flowerAnimator.SetTrigger("Close");
        }
        else
        {
            _flowerAnimator.ResetTrigger("Close");
            _flowerAnimator.SetTrigger("Open");
        }
    }
}
