using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowController : MonoBehaviour
{
    [SerializeField] Camera _mainCamera;
    [SerializeField] Transform _followTarget;
    [SerializeField] float _cameraSpeed = 1f;
    private Vector3 _startingPosition;
    private Vector3 _endPosition;

    private void Start()
    {
        _startingPosition = transform.position;
    }

    private void Update()
    {
        if (GameController.Instance.IsFlowerAlive)
        {
            transform.position = new Vector3(transform.position.x, _followTarget.position.y+3, transform.position.z);
            _endPosition = transform.position / 2f;
        }
        else
        { 
            if (Vector3.Distance(transform.position, _endPosition) > 0.01f)
            {
                var step = _cameraSpeed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, _endPosition, step);
                _mainCamera.orthographicSize = Mathf.MoveTowards(_mainCamera.orthographicSize, _endPosition.y*1.1f, step);

            }
        }
    }

}
