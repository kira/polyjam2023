using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceController : MonoBehaviour
{
    private AudioClip audio;

    private float aCertainLoudThreshold = 0.2f;
    private float aCertainSiltenThreshold =  0.1f;

    public string result = "";
    public float value = 0;
    public float avarageValue = 0;

    
    private float y =0;
    public LineRenderer renderer;

    private float time = 0;

    public int loudConut = 0;
    public int silentCount = 0;

    public static EResult Result = EResult.NONE;

    public EResult CurrentResult;
    
    public enum EResult
    {
        NONE,
        SILENT,
        LOUD
    }
    
    void Start()
    {
        foreach (var tmp in Microphone.devices)
        {
            Debug.Log("devices name: " + tmp);
        }
        
        audio = Microphone.Start(Microphone.devices[0], true, 1, 16000);
    }
    
    void Update()
    {
        this.gameObject.SetActive(GameController.Instance.IsFlowerAlive);
        
        //
        var fdata = new float[audio.samples * audio.channels];

        var currentIndex = Microphone.GetPosition(null);
        audio.GetData(fdata, currentIndex);
        
        value = fdata[fdata.Length - 1];

        y += Time.deltaTime;
        renderer.positionCount = renderer.positionCount + 1;
        renderer.SetPosition(renderer.positionCount - 1, new Vector3(y, value));
        if (y > 20)
        {
            var position = renderer.transform.position;
            position.x = renderer.transform.position.x - Time.deltaTime;
            renderer.transform.position = position;
        }

        time += Time.deltaTime;
        if (time < 0.3) return;

        time = 0;
        //Debug.Log("read");
        
        avarageValue = avarage(fdata);
        
        if (Mathf.Abs(avarageValue) >= aCertainLoudThreshold)
        {
            result = "loud";
            loudConut++;
            silentCount = 0;
        }
        else if (Mathf.Abs(avarageValue) >= aCertainSiltenThreshold)
        {
            result = "silent";
            silentCount++;
            loudConut = 0;
            
        }
        else
        {
            result = "none";
            silentCount = 0;
            loudConut = 0;
        }

        if (loudConut >= 2)
        {
            Result = EResult.LOUD;
            loudConut = 0;
        }
        else if (silentCount >= 2)
        {
            Result = EResult.SILENT;
            silentCount = 0;
        }
        else
        {
            Result = EResult.NONE;
        }

        CurrentResult = Result;
    }

    private float avarage(float[] data)
    {
        float sum = 0;
        for (int i = 0; i < data.Length; i++)
        {
            sum += Mathf.Abs(data[i]);
        }

        return sum / data.Length;
    }

    public void Exit()
    {
        this.transform.gameObject.SetActive(false);
    }
}
